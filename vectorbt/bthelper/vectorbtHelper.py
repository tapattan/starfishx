'''
date: 2022-04-03
version 0.101
suport : and / or 
ind : sma , macd , rsi
'''
import json
import pandas as pd
import numpy as np
import matplotlib_inline
import matplotlib.pyplot as plt
matplotlib_inline.backend_inline.set_matplotlib_formats('svg')

def loadHistPrice(data):
  contentLoad = ''  
  timeframe = 'd'
  if(type(data)==str): # ถ้าเป็น str มา จะใช้ตลาดที่ไทย และเป็น timeframe 1d 
     contentLoad += "tv = TvDatafeed()"+"\n"
     contentLoad += "price = tv.get_hist(symbol='"+data+"',exchange='SET',interval=Interval.in_daily,n_bars=5000)"+"\n"
     contentLoad += "price = price['close']"+"\n"
    
  elif(type(data)==dict):
     symbol = data['symbol']
     market = data['exchange']
     tf = data['timeframe']
     
     if(tf=='1d'):   
       tf = 'Interval.in_daily'
     elif(tf=='1w'):
       tf = 'Interval.in_weekly'
       timeframe = 'w'
     elif(tf=='1m'):
       tf = 'Interval.in_monthly'
       timeframe = 'm'
     else:
       timeframe = 'Error'
       return 'Error : TimeFrame Not Found (Valid intervals :[1d,1w,1m] )'  

     contentLoad += "tv = TvDatafeed()"+"\n"
     contentLoad += "price = tv.get_hist(symbol='"+symbol+"',exchange='"+market+"',interval="+tf+",n_bars=5000)"+"\n"
     contentLoad += "price = price['close']"+"\n"
    
  return contentLoad,timeframe

def import_module():
    m = ["import sys","import os","import vectorbt as vbt","import pandas as pd","import numpy as np",
         "from tvDatafeed import TvDatafeed, Interval",
         "import matplotlib_inline",
         "import matplotlib.pyplot as plt",
         "matplotlib_inline.backend_inline.set_matplotlib_formats('svg')"]
    
    header = ''
    for i in m:
      header+=i+'\n'
    
    header+='try:'+"\n"
    header+='  import bthelper as bth'+"\n"
    header+='except:'+"\n"
    header+="  sys.path.insert(0, './')"+"\n"
    header+="  import bthelper as bth"+"\n"
 

    return header+'\n'

def check_strategy(data):
    content = ''
    startdate =  data['startdate'] 
    enddate =  data['enddate']

    budget = str(data['budget'])
    fees = str(data['fees'])
    # plugin load data (history price)
    _,timeframe = loadHistPrice(data['asset']) 
    content += _

    #content += "price = vbt.YFData.download('BTC-USD').get('Close')"+"\n"
    content += "price = price[price.index >= '"+startdate+"']"+"\n"
    content += "price = price[price.index <= '"+enddate+"']"+"\n"

    #ป้องกันตัวเชื่อมผิด
    conj = get_conjunction(data['condition-entries'])
    conj_exit = get_conjunction(data['condition-exits'])
    if(conj=='BOTH' or conj_exit=='BOTH'):
      return 'not-support' 
    
    

    if(type(data['condition-entries']) == dict): 
      content += '\n#condition-entries\n'
      if(conj=='NO-Conj'): # คือมี indicator แต่ไม่มี and เชื่อมมา
      
          startdate =  data['startdate'] 
          enddate = data['enddate']
       
          indicator = getFirstIndex(data['condition-entries'])
          if(indicator == 'indicator_sma'):
    
            k1 = str(data['condition-entries']['indicator_sma']['fast'])
            k2 = str(data['condition-entries']['indicator_sma']['slow'])
          
            if('close' in k1): # เป็นกรณีที่ใช้เส้น sma เส้นเดียว ฝั่งซ้ายมือคือเส้นค่าเฉลี่ยสั้นเสมอ
              
              content += "sma"+k2+" = vbt.MA.run(price, "+k2+")"+"\n"
              content += "entries = "+"sma"+k2+".ma_below(price)"+"\n"
              #line6 = "exits = smaDays.ma_above(price)"+"\n"
              #line7 = "pf_kwargs = dict(fixed_fees=0)"+"\n"

            elif(not ('close' in k1 or 'close' in k2)): #ระบุเส้นค่าเฉลี่ยมา 

              content += "sma"+k1+" = vbt.MA.run(price, "+k1+")"+"\n"
              content += "sma"+k2+" = vbt.MA.run(price, "+k2+")"+"\n"

              content += "entries = sma"+k1+".ma_crossed_above("+"sma"+k2+")"+"\n"
              #line7 = "exits = fast_ma.ma_crossed_below(slow_ma)"+"\n"

          if(indicator == 'indicator_rsi'):

            buyat = str(data['condition-entries'][indicator]['level'])
            #sellat = str(data['condition-entries'][indicator]['sell'])
            content += "rsi_line = vbt.RSI.run(price,window=14)"+"\n"    
            content += "entries = rsi_line.rsi_below("+buyat+")"+"\n"

          if(indicator == 'indicator_macd'):
            content = ''
            content += "price = vbt.YFData.download('BTC-USD').get('Close')"+"\n"
            content += "price = price[price.index >= '2023-01-01']"+"\n"
            content += "price = price[price.index <= '2023-03-31']"+"\n"
            content += "\n#compute MACD"+"\n"

            p1 = str(data['condition-entries'][indicator]['fast'])
            p2 = str(data['condition-entries'][indicator]['slow'])
            
            content += "macd_ind = vbt.MACD.run(price, fast_window=12, slow_window=26, signal_window=9)"+"\n"
            if(p1=='MACD' and p2=='SIGNAL'):
              content += "entries = macd_ind.macd_above(macd_ind.signal)"+"\n"
              #content += "exits = macd_ind.macd_below(macd_ind.signal)"+"\n"
            elif(p1=='MACD' and p2=='0'):
              content += "entries = macd_ind.macd_above(0)"+"\n"
              #content += "exits = macd_ind.macd_below(0)"+"\n"
            
        
            
      if(conj=='AND' or conj=='OR'): #คือมี and condition
       
        conNo = 1
        signalListEntry = []
        
        for i in data['condition-entries'][conj]:
          k = getFirstIndex(i)
          #print(i)
          if(k=='indicator_sma'):
            p1 = str(i['indicator_sma']['fast'])
            p2 = str(i['indicator_sma']['slow'])
            
            if(p1=='close'):
              content += ""
              content += "sma"+p2+" = vbt.MA.run(price, "+p2+")"+"\n"
              content += "entries"+str(conNo)+" = sma"+p2+".ma_below(price)"+"\n"
              #code4 = "exits"+str(conNo)+" = sma"+p2+".ma_above(price)"+"\n"
            else:
              content += "sma"+p1+" = vbt.MA.run(price, "+p1+")"+"\n"
              content += "sma"+p2+" = vbt.MA.run(price, "+p2+")"+"\n"
              content += "entries"+str(conNo)+" = sma"+p1+".ma_crossed_above(sma"+p2+")"+"\n"
              #code4 = "exits"+str(conNo)+" = sma"+p1+".ma_crossed_below(sma"+p2+")"+"\n"
          
          if(k=='indicator_rsi'): #กรณีเงื่อนไขเป็น RSI
              level = str(i['indicator_rsi']['level'])
            
              content += "rsi_line = vbt.RSI.run(price,window=14)"+"\n"
              content += "entries"+str(conNo)+" = rsi_line.rsi_below("+level+")"+"\n"
              #code4 = "exits"+str(conNo)+" = rsi_line.rsi_above("+sellat+")"+"\n"
          
          if(k=='indicator_macd'):
              p1 = str(i['indicator_macd']['fast'])
              p2 = str(i['indicator_macd']['slow'])
              
              content += "macd_ind = vbt.MACD.run(price, fast_window=12, slow_window=26, signal_window=9)"+"\n"
            
              if(p1=='MACD' and p2=='SIGNAL'):
                content += "entries"+str(conNo)+" = macd_ind.macd_above(macd_ind.signal)"+"\n"
                #code4 = "exits"+str(conNo)+" = macd_ind.macd_below(macd_ind.signal)"+"\n"
              elif(p1=='MACD' and p2=='0'):
                content += "entries"+str(conNo)+" = macd_ind.macd_above(0)"+"\n"
                #code4 = "exits"+str(conNo)+" = macd_ind.macd_below(0)"+"\n"  

        
          
          signalListEntry.append("entries"+str(conNo))

          conNo+=1  

 
        
        tmp_entry = 'entries = '
        for i in range(len(signalListEntry)):
          if(i!=len(signalListEntry)-1):
            m = ''
            if(conj=='AND'):
              m = ' ^ '
            else:
              m = ' | '
              
            tmp_entry += signalListEntry[i]+m
          else:
            tmp_entry += signalListEntry[i] 
          
        content+=tmp_entry+"\n \n"

      
    

    ###### ส่วนสัญญาออก
    
    if(type(data['condition-exits']) == dict): 
      content += "\n\n\n#condition-exits\n"
      if(conj_exit=='NO-Conj'):  #ไม่มี AND มา ไม่มี OR มา
        indicator = getFirstIndex(data['condition-exits'])
        if(indicator == 'indicator_sma'):
          
          k1 = str(data['condition-exits']['indicator_sma']['fast'])
          k2 = str(data['condition-exits']['indicator_sma']['slow'])
            
          if('close' in k1): # เป็นกรณีที่ใช้เส้น sma เส้นเดียว ฝั่งซ้ายมือคือเส้นค่าเฉลี่ยสั้นเสมอ
            content += "sma"+k2+" = vbt.MA.run(price, "+k2+")"+"\n"
            content += "exits = "+"sma"+k2+".ma_above(price)"+"\n"

          elif(not ('close' in k1 or 'close' in k2)): #ระบุเส้นค่าเฉลี่ยมา 
        

              content += "sma"+k1+" = vbt.MA.run(price, "+k1+")"+"\n"
              content += "sma"+k2+" = vbt.MA.run(price, "+k2+")"+"\n"

              content += "exits = sma"+k1+".ma_crossed_below("+"sma"+k2+")"+"\n"
              #line7 = "exits = fast_ma.ma_crossed_below(slow_ma)"+"\n"  
               
        if(indicator == 'indicator_rsi'):

            #buyat = str(data['condition-entries'][indicator]['level'])
            sellat = str(data['condition-exits'][indicator]['level'])
            content += "rsi_line = vbt.RSI.run(price,window=14)"+"\n"
            content += "exits = rsi_line.rsi_below("+sellat+")"+"\n"
        
        if(indicator == 'indicator_macd'):
           
            p1 = str(data['condition-exits'][indicator]['fast'])
            p2 = str(data['condition-exits'][indicator]['slow'])
              
            content += "macd_ind = vbt.MACD.run(price, fast_window=12, slow_window=26, signal_window=9)"+"\n"
            if(p1=='MACD' and p2=='SIGNAL'):
                #content += "entries"+str(conNo)+" = macd_ind.macd_above(macd_ind.signal)"+"\n"
                content += "exits = macd_ind.macd_below(macd_ind.signal)"+"\n"
            elif(p1=='MACD' and p2=='0'):
                #content += "entries"+str(conNo)+" = macd_ind.macd_above(0)"+"\n"
                content += "exits = macd_ind.macd_below(0)"+"\n"

      
      if(conj_exit=='AND' or conj_exit=='OR'): #คือมี and condition
      
        conNo = 1
        signalListExit = []
        for i in data['condition-exits'][conj_exit]:
          k = getFirstIndex(i)
          #print(i)
          if(k=='indicator_sma'):
            p1 = str(i['indicator_sma']['fast'])
            p2 = str(i['indicator_sma']['slow'])
            
            if(p1=='close'):
              content += ""
              content +=  "sma"+p2+" = vbt.MA.run(price, "+p2+")"+"\n"
              #code3 = "entries"+str(conNo)+" = sma"+p2+".ma_below(price)"+"\n"
              content += "exits"+str(conNo)+" = sma"+p2+".ma_above(price)"+"\n"
            else:
              content += "sma"+p1+" = vbt.MA.run(price, "+p1+")"+"\n"
              content += "sma"+p2+" = vbt.MA.run(price, "+p2+")"+"\n"
              #code3 = "entries"+str(conNo)+" = sma"+p1+".ma_crossed_above(sma"+p2+")"+"\n"
              content += "exits"+str(conNo)+" = sma"+p1+".ma_crossed_below(sma"+p2+")"+"\n"
          
          if(k=='indicator_rsi'): #กรณีเงื่อนไขเป็น RSI
              level = str(i['indicator_rsi']['level'])
              
              content += "rsi_line = vbt.RSI.run(price,window=14)"+"\n"
              #code3 = "entries"+str(conNo)+" = rsi_line.rsi_below("+buyat+")"+"\n"
              content += "exits"+str(conNo)+" = rsi_line.rsi_above("+level+")"+"\n"
          
          if(k=='indicator_macd'):
              p1 = str(i['indicator_macd']['fast'])
              p2 = str(i['indicator_macd']['slow'])
              
              content += "macd_ind = vbt.MACD.run(price, fast_window=12, slow_window=26, signal_window=9)"+"\n"
              
              if(p1=='MACD' and p2=='SIGNAL'):
                #code3 = "entries"+str(conNo)+" = macd_ind.macd_above(macd_ind.signal)"+"\n"
                content += "exits"+str(conNo)+" = macd_ind.macd_below(macd_ind.signal)"+"\n"
              elif(p1=='MACD' and p2=='0'):
                #code3 = "entries"+str(conNo)+" = macd_ind.macd_above(0)"+"\n"
                content += "exits"+str(conNo)+" = macd_ind.macd_below(0)"+"\n"  

       
          
          #signalListEntry.append("entries"+str(conNo))
          signalListExit.append("exits"+str(conNo))
          
         
              
          conNo+=1  

        content += ''
 

        tmp_entry = 'exits = '
        for i in range(len(signalListExit)):
          if(i!=len(signalListExit)-1):
            m = ''
            if(conj_exit=='AND'):
              m = ' ^ '
            else:
              m = ' | '
            tmp_entry += signalListExit[i]+m
          else:
            tmp_entry += signalListExit[i] 
          
        content+=tmp_entry+"\n\n"
        
       
    ###### part process and MM
    content += "   \n"


    ###### part of money management 
    mm = get_money_management(data)
 

    if(data['condition-entries'] == "None" and data['condition-exits'] == "None"): #buy and hold

      content += "" #do nothing 
      content += "pf = vbt.Portfolio.from_holding(price, init_cash="+budget+mm+",fees="+fees+",freq='"+timeframe+"')"+"\n"
    else:
      content += "pf = vbt.Portfolio.from_signals(price, entries, exits, init_cash="+budget+mm+",fees="+fees+",freq='"+timeframe+"')"+"\n"

    #content += "result = pf.total_profit()"+"\n"
    #content += "print(pf.stats())"+"\n"
    
    #content += "\n\npf.plot(subplots = ['cum_returns', 'orders', 'trade_pnl','underwater'])"
    
    ##### ส่วนของรายงาน
    content += "\n#part report"+"\n"

    filename = data['account']+'-'+data['strategy-name']
    path = data['info']['path_w']
    path_write = path+'/'+filename
    content += part_report(data['report'],path_write)
    
    content += "pf.positions.records_readable.to_csv('log.csv')"+"\n"
    return remove_lin_dup(content)

def write_python_file(account,contentfile):
    f = open(account+".py", "w")
    f.write(contentfile)
    f.close()
    return account+".py"


def remove_lin_dup(content):
    mm = content.split('\n')
    c = [] 
    new_mm = ''
    for i in mm:
      if(not i in c):
        c.append(i)
        new_mm+=i+"\n"

    return new_mm  

def get_conjunction(p):
  if(p=='None'):
     return 'No-Conj'
  
  if(p.get('AND',False) and p.get('OR',False)):
     return 'BOTH'
  elif(p.get('AND',False)):
     return 'AND'
  elif(p.get('OR',False)):
     return 'OR'  
  else:
     return 'NO-Conj'
     
# ยกเลิกใช้แล้ว
'''def genOutput(data):
    r = ''
    for i in data:
      r+=i 
    return r'''

def getFirstIndex(data):
    if(type(data)==dict):
      if(len(data)==1):  
        for i in data:
         return i
        
def gen_py_from_json(filename):
    try:
      f = open(filename, "r")
      try:
        # Reading from file
        try:
          tmp = f.read()
          tmp = tmp.replace("'","\"")

          data = json.loads(tmp)
        except:
          print('502:Can Not Load Json File')  
        header = import_module()
        
        body = check_strategy(data)
        contentfile = header+body
        filename = data['account']+'-'+data['strategy-name']
        path = data['info']['path_w']
        f = write_python_file(path+'/'+filename,contentfile)
        print('success: '+f)
        return f
      except:
        print('501:Parser Error')
    except:
      print('404:File Not Found')


def part_report(report,path):  
    '''
    กรณีต้องการกราฟด้วย
    {"report":["stats","cum_returns", "orders", "trade_pnl","underwater"]} 
    หรือ 
    กรณีต้องการแต่สถิติ
    {"report":"stats"}
    '''
    content = ""
    if(type(report)==str):
      if(report=='stats'):  
        content += "print(pf.stats())"+"\n"
        content += "bth.gen_stats(pf.stats(),'"+path+"-stats.png')"+"\n"

    elif(type(report)==list):
       for i in report:
         if(i=='stats'):  
            content += "print(pf.stats())"+"\n"
            content += "bth.gen_stats(pf.stats(),'"+path+"-stats.png')"+"\n"
         elif(i=='cum_returns'):
            content +="fig = pf.plot_cum_returns(title='cum_returns')"+"\n"
            content +="fig.write_image('"+path+"-cum_returns.png')"+"\n\n"
         elif(i=='orders'):
            content +="fig = pf.plot_orders(title='orders')"+"\n"
            content +="fig.write_image('"+path+"-orders.png')"+"\n\n"
         elif(i=='trade_pnl'):
            content +="fig = pf.plot_trade_pnl(title='trade_pnl')"+"\n"
            content +="fig.write_image('"+path+"-trade_pnl.png')"+"\n\n"
         elif(i=='underwater'):
            content +="fig = pf.plot_underwater(title='underwater')"+"\n"
            content +="fig.write_image('"+path+"-underwater.png')"+"\n\n"

    return content


def gen_stats(pf,filename):
    try:
        df = pd.DataFrame(pf)
        df = df.reset_index()
        k = [] 
        for i in range(len(df[0])):
           if(type(df[0].iloc[i])==np.float64):
             k.append(round(df[0].iloc[i],3))
           else:
             m = str(df[0].iloc[i])
             m = m.replace('09:00:00','')
             m = m.replace('00:00:00','')
             p = m.split(' ')
             if(len(p)>1):
               m = p[0]+' '+p[1]
             else:
               m = p[0]     
             k.append(m)

        df['stat'] = k
        df = df[['index','stat']]
        
        # Create a plot of the DataFrame
        fig = plt.figure(figsize=(20, 30))
     
        fig, ax = plt.subplots()
        ax.axis('off')
        m = ax.table(cellText=df.values, colLabels=df.columns,loc='center')
        # Save the plot as a PNG image
        plt.savefig(filename,bbox_inches = 'tight',dpi=400)
        return True
    except:
        return False
    

def get_money_management(data):
   p = data.get('moneymanagement',False)
   if(p==False):
      return '' 
   elif(type(p)==dict):
      if(getFirstIndex(p)=='percent_port'):
         k = p['percent_port']
         return ",size="+str(k)+",size_type=2"
      elif(getFirstIndex(p)=='fix_value'):
         k = p['fix_value']
         return ",size="+str(k)
      else:
         return ''
   else:
      return ''
   
 
 