import vectorbt as vbt
import pandas as pd

price = vbt.YFData.download('BTC-USD').get('Close')
price = price[price.index >= '2018-01-01']
price = price[price.index <= '2023-03-31']

#condition-entries
sma10 = vbt.MA.run(price, 10)
entries1 = sma10.ma_below(price)
sma50 = vbt.MA.run(price, 50)
entries2 = sma10.ma_crossed_above(sma50)
sma75 = vbt.MA.run(price, 75)
entries3 = sma50.ma_crossed_above(sma75)
sma200 = vbt.MA.run(price, 200)
entries4 = sma75.ma_crossed_above(sma200)
rsi_line = vbt.RSI.run(price,window=14)
entries5 = rsi_line.rsi_below(30)
entries = entries1 ^ entries2 ^ entries3 ^ entries4 ^ entries5
 
#condition-exits
exits1 = sma200.ma_above(price)
exits2 = rsi_line.rsi_above(80)
exits = exits1 | exits2
   
pf = vbt.Portfolio.from_holding(price, entries, exits, init_cash=100,fees=0.01)
result = pf.total_profit()
print(result)
