import vectorbt as vbt
import pandas as pd

price = vbt.YFData.download('BTC-USD').get('Close')
price = price[price.index >= '2018-01-01']
price = price[price.index <= '2023-03-31']

#condition-entries
pf = vbt.Portfolio.from_holding(price, init_cash=100,fees=0.01)
result = pf.total_profit()
print(result)
