import unittest
import pandas as pd

def sma_crossover(data, short_window, long_window):
    # Calculate short and long simple moving averages
    short_sma = data.rolling(window=short_window).mean()
    long_sma = data.rolling(window=long_window).mean()

    # Generate signals where short SMA crosses over long SMA
    signals = pd.Series(0, index=data.index)
    signals[short_sma > long_sma] = -1
    signals[short_sma < long_sma] = 1

    return signals

class TestSmaCrossover(unittest.TestCase):
    def setUp(self):
        self.data = pd.Series([10, 20, 30, 25, 35, 40, 30, 25, 20])
        self.short_window = 2
        self.long_window = 5
        
    def test_sma_crossover(self):
        expected_signals = pd.Series([0, 0, 0, 0, -1, -1, -1, 1, 1], index=self.data.index)
        actual_signals = sma_crossover(self.data, self.short_window, self.long_window)
        print(actual_signals)
        pd.testing.assert_series_equal(actual_signals, expected_signals)
        
if __name__ == '__main__':
    unittest.main()
