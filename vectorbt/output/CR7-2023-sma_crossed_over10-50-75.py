import vectorbt as vbt
import pandas as pd

price = vbt.YFData.download('BTC-USD').get('Close')
price = price[price.index >= '2018-01-01']
price = price[price.index <= '2023-03-31']

#condition-entries
sma20 = vbt.MA.run(price, 20)
sma50 = vbt.MA.run(price, 50)
entries1 = sma20.ma_crossed_above(sma50)
sma75 = vbt.MA.run(price, 75)
entries2 = sma50.ma_crossed_above(sma75)
entries = entries1 ^ entries2
 
#condition-exits
exits1 = sma20.ma_crossed_below(sma50)
exits2 = sma50.ma_crossed_below(sma75)
exits = exits1 ^ exits2
   
pf = vbt.Portfolio.from_holding(price, entries, exits, init_cash=100,fees=0.01)
result = pf.total_profit()
print(result)
