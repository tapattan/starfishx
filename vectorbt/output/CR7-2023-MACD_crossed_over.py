import vectorbt as vbt
import pandas as pd

price = vbt.YFData.download('BTC-USD').get('Close')
price = price[price.index >= '2018-01-01']
price = price[price.index <= '2023-03-31']

#condition-entries
macd_ind = vbt.MACD.run(price, fast_window=12, slow_window=26, signal_window=9)
entries1 = macd_ind.macd_above(macd_ind.signal)
entries2 = macd_ind.macd_above(0)
entries = entries1 ^ entries2
 
#condition-exits
exits1 = macd_ind.macd_below(macd_ind.signal)
exits2 = macd_ind.macd_below(0)
exits = exits1 | exits2
   
pf = vbt.Portfolio.from_holding(price, entries, exits, init_cash=100,fees=0.01)
result = pf.total_profit()
print(result)
