import vectorbt as vbt
import pandas as pd

price = vbt.YFData.download('BTC-USD').get('Close')
price = price[price.index >= '2018-01-01']
price = price[price.index <= '2023-03-31']

#condition-entries
sma10 = vbt.MA.run(price, 10)
sma50 = vbt.MA.run(price, 50)
entries = sma10.ma_crossed_above(sma50)
#condition-exits
sma200 = vbt.MA.run(price, 200)
exits = sma200.ma_above(price)
   
pf = vbt.Portfolio.from_holding(price, entries, exits, init_cash=100,fees=0.01)
result = pf.total_profit()
print(result)
