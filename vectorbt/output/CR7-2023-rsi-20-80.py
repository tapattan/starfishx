import vectorbt as vbt
import pandas as pd

price = vbt.YFData.download('BTC-USD').get('Close')
price = price[price.index >= '2018-01-01']
price = price[price.index <= '2023-03-31']

#condition-entries
rsi_line = vbt.RSI.run(price,window=14)
entries = rsi_line.rsi_below(20)
#condition-exits
exits = rsi_line.rsi_below(80)
   
pf = vbt.Portfolio.from_holding(price, entries, exits, init_cash=100,fees=0.01)
result = pf.total_profit()
print(result)
