import vectorbt as vbt
import pandas as pd

price = vbt.YFData.download('BTC-USD').get('Close')
price = price[price.index >= '2023-01-01']
price = price[price.index <= '2023-03-31']

#compute MACD
macd_ind = vbt.MACD.run(price, fast_window=12, slow_window=26, signal_window=9)
entries = macd_ind.macd_above(macd_ind.signal)
#condition-exits
exits = macd_ind.macd_below(0)
   
pf = vbt.Portfolio.from_holding(price, entries, exits, init_cash=100,fees=0.01)
result = pf.total_profit()
print(result)
